# Inrtroduction

This website is the port of `https://gitlab.com/piratemovin/website` to codeigniter, a popular PHP MVC framework. You can see the live website at [pirates.org.in](http://piraets.org.in). 

# MVC (Model-View-Controller)

* **Model**: The model directly manages the data, logic and rules of the application.
* **View**: The view is the information that is being presented to a user.  It'll normally be a full web page or fragments such as header, footer, etc.
* **Controller**:  The controller accepts input and converts it to commands for the model or view.

[Codeigniter Official Website](https://www.codeigniter.com/)  
[Codeigniter Official Documentation](http://www.codeigniter.com/user_guide/)

# Basic structure

* `/application/` contains models, views and controllers.
* `/assets/` contains stylesheets, javascripts and images.
* `/system/` contains codeigniter core codes, libraries, helpers and other files.

# Working explained

* Controllers contain functions which will load various views.
* Upon visiting the URL `http://www.pirates.org.in` the default controller is called automatically. Default controller is configured in the `/application/config/config.php`.
* The default controller is set to `/application/controllers/Home.php` (note the capital `'H'` in `Home.php`, controller names should begin with a capital letter) which will load the template view `/application/views/template/default.php`.
* Template view in turn calls other views called header, content and footer respectively.
* Header and footer views in the template are fixed while the content view varies based on the controller function. For eg. the function named `'kerala2016'` in the `/application/controller/Elections.php` controller loads the view `/application/views/elections/kerala2016.php` as content.

# Contributing

For contributing kindly follow the steps below

## Prerequisites (Recommended)

* [LAMP](https://www.linux.com/learn/easy-lamp-server-installation)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Getting started

* Fork [this](https://gitlab.com/ambadyanands2207/piratesite) repo (See official gitlab documentation [here](https://docs.gitlab.com/ce/workflow/forking_workflow.html) to know how)
* Clone the forked repo (i.e. create a local copy of the repo on your computer)
`git clone https://gitlab.com/<your_username>/piratesite.git`
*(NOTE: Replace* `<your_username>` *part with your real gitlab username)*
* Navigate to the `config` directory
`cd piratesite/application/config`
* Open `config.php` in a text-editor
* In the line starting with `$config['base_url']` replace `http://pirates.org.in/` with `http://localhost/` so that it will look like this:
`$config['base_url']='http://localhost/';`


## How to add a new webpage

*(NOTE: You don't need to know PHP or codeigniter in order to do this, anyone who is interested and having a basic knowledge in HTML can do!)*  

Suppose you want to publish a new webpage about 'encryption' and access it at `http://pirates.org.in/tools/encryption`. Let's check how to do this:  

**Step 1.** Open the file `Template.php` located at `/application/controllers/templates/` with a text-editor.

**Step 2.** In the line starting with `class Template`, rename `Template` to `Encryption`.

**Step 3.** In the line starting with `$data['content']`, rename `templates/page` to `tools/encryption` so that it will look like this:  
`$data['content'] = "tools/encryption";`.

**Step 4.** Save the current file as `Encryption.php` inside `/application/controllers/`  
*(NOTE: This filename and the class name given in Step 2 should be the same and it should begin with a capital letter)*  

**Step 5.** Create a new folder named `tools` under `/application/views/` and inside that `tools` folder create a new file named `encryption.php`. Open it with a text-editor and insert the HTML content you want to create the webpage.  
(*Note: You don't need to add the tags `<html>`, `<head>` or `<body>` in this file as they will be automatically loaded from the header template. All you need to enter is the content between `<body>` and `</body>`)*


# More Details

* Domain name registered by Sooraj Kenoth (managed by Manukrishnan TV, renewal sponsored by Hash Singularity). 
* Hosted on [diasp.in](htps://diasp.in) server (managed by Praveen, Akshay and Bady).
* Access managed at https://gitlab.com/piratemovin/access (private repo).
* To contribute to the website either create a pull request or request access to the [Indian Pirates Web Team](https://www.loomio.org/g/uyHjLtKn/indian-pirates-web-team).
* Once you have joined the web team and got ssh access to the server, you can modify the live website by running `git pull` on the server.

# Contact

* To join the Indian Pirates please go to the loomio group [here](https://www.loomio.org/g/7qmru1SG/indian-pirates).  
* You can join the matrix room at [#piratesin:diasp.in](https://matrix.to/#/#piratesin:diasp.in). 
* If you have an XMPP ID you can also join the Indian Pirate Conference at `piratesin@conference.diasp.in` (Know more about how to get XMPP ID [here](http://pirates.org.in/social/xmpp))
