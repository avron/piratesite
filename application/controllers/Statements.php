<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statements extends CI_Controller {

	public function censorship() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/censorship'; //This corresponds to /application/views/statements/censorship.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
  }

	public function letter_to_arvind_kejriwal() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/letter-to-arvind-kejriwal'; //This corresponds to /application/views/statements/letter-to-arvind-kejriwal.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

	public function blackmoney() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/blackmoney'; //This corresponds to /application/views/statements/letter-to-arvind-kejriwal.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

  public function say_no_to_war() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/say-no-to-war'; //This corresponds to /application/views/statements/say-no-to-war.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

  public function free_olabini() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/free-olabini'; //This corresponds to /application/views/statements/free-olabini.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

}
