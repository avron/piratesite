<section id="home">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
          <div class="row text-center main">
            <h4 class="recent">
              Most Recent
              <br>
              <a href="statements/free-olabini">Free Ola Bini Solidarity Letter</a>
              <br>
              <a href="statements/say-no-to-war">Indian Pirates endorse petition asking India and Pakistan to avoid further hostilities</a>
              <br>
              <a href="https://rethinkaadhaar.in/">Indian Pirates support the campaign Rethink Aadhaar</a>
              <br>
              <a href="https://keepusonline.in">Sign the petition to demand an end to arbitrary Internet shutdowns</a></h4>
            <h2><a href="https://www.loomio.org/d/SFfYwagX/membership-requests" target="_blank">Join Indian Pirates</a> in our fight for changing things around us for the good!</h2>
            <h3>Indian Pirates are committed to support human rights, direct democracy and participation in government, reform of copyright and patent law, free sharing of knowledge (open content), information privacy, transparency, freedom of information, anti-corruption and network neutrality.</h3>
          </div>
        </div>
      </div>
      <div class="row" id="quotes">
        <div class="col-md-4 col-md-offset-1">
          <blockquote>We should no longer accept what we supposedly cannot change. Instead, we need to change what we cannot accept.</blockquote>
        </div>
        <div class="col-md-4 col-md-offset-2">
          <blockquote>Order followers are the equivalent of modern day house slaves, who would rather enslave their brothers and sisters, rather than grow up, develop real courage, and think for themselves<footer>Mark Passio</footer></blockquote>
        </div>
      </div>
      <div id="whatpirates">
        <div class="row">
          <h2 class="text-center">Pirates are freedom fighters of Digital Age</h2>
        </div>
        <div class="row text-center">
          <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <a href="https://www.loomio.org/d/SFfYwagX/membership-requests" target="_blank">
              <button class="btn btn-warning">Be a Pirate</button>
            </a>
            <a href="constitution/" target="_blank">
              <button class="btn btn-warning">Our Constitution</button>
            </a>
            <a href="https://riot.im/app/#/room/#piratesin:matrix.org" target="_blank">
              <button id="riot-button" class="btn btn-warning">Join the Conversation</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="links" class="row">
  <div id="statements" class="col-sm-4 col-sm-offset-1">
    <h3 class="text-center">Statements</h3>
    <ul>
      <li><a href="statements/blackmoney">Demands for effective curbing of black money</a></li>
      <li><a href="statements/letter-to-arvind-kejriwal">Open Letter to Arvind Kejriwal on his all male cabinet</a></li>
      <li><a href="statements/censorship">On Indian Government Censorship of the Internet</a></li>
    </ul>
  </div>
  <div id="discussions" class="col-sm-4 col-sm-offset-2">
    <h3 class="text-center">Discussions</h3>
    <ul>
      <li><a href="https://www.loomio.org/d/0wSn9fXY/india-s-daughter-and-censorship"> India's Daughter and Censorship</a></li>
    </ul>
  </div>
</section>
<section id="constitution">
  <iframe src="<?php echo base_url(); ?>constitution/" frameborder="0"></iframe>
</section>
