<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo isset($title) ? $title." | Indian Pirates" : "Indian Pirates" ;?></title>
	<meta name="description" content="<?php echo isset($description) ? $description." | Indian Pirates" : "The Official Website of Indian Pirates" ;?>">
	<meta name="author" content="<?php echo isset($author) ? $author." | Indian Pirates" : "Indian Pirates Web Team" ;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
	<script defer src="<?php echo base_url(); ?>assets/js/jquery.min.js" type="text/javascript"></script>
	<script defer src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
	<div id="body-container" class="container-fluid">
		<nav class="navbar navbar-inverse">
	   		<div class="navbar-header">
	    		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">
			    	<span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
			    </button>
	      		<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/IndianPiratesLogo.svg" height="50px" alt="Logo of Indian Pirates" /></a>
    		</div>
    		<div class="collapse navbar-collapse" id="navbar-main">
      			<ul class="nav navbar-nav navbar-right">
	        		<li><a href="<?php echo base_url(); ?>">Home<span class="sr-only">(current)</span></a></li>
	       			<li><a href="<?php echo base_url(); ?>constitution/">Constitution</a></li>
					<li><a href="<?php echo base_url(); ?>manifesto/">Manifesto</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Elections <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" id="elections">
								<li><a href="<?php echo base_url(); ?>elections/loksabha2019">Lok Sabha Election 2019</a></li>
								<li><a href="<?php echo base_url(); ?>elections/kerala2016">Kerala Assembly Election 2016</a></li>
						</ul>
					</li>
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Campaigns <span class="caret"></span></a>
		        	<ul class="dropdown-menu" role="menu" id="campaigns">
		            <li><a href="<?php echo base_url(); ?>cycling">Pirate Cycling</a></li>
		            <li><a href="http://yatra.diasporafoundation.org" target="_blank">Diaspora Yatra</a></li>
		            <li><a href="https://otr.works/" target="_blank">OTR Works</a></li>
		        	</ul>
		        </li>
        		<li> <a href="https://www.loomio.org/d/SFfYwagX/membership-requests" id="joinbutton" class="btn btn-warning" target="_blank">Be a Pirate</a> </li>
	     	 	</ul>
	    	</div>
		</nav>
