<style type="text/css">
 @import url("<?php echo base_url(); ?>assets/css/page.css");
</style>
<section id="statement" class="page">
  <h2>Indian Pirates endorse the following petition, asking India and Pakistan to avoid further hostilities, as seen in <a href="https://thewire.in/rights/full-text-citizens-group-asks-india-pakistan-to-avoid-further-hostilities" target="_blank">The Wire</a> Website</h2>
  <h5 class="date">March 6, 2019</h5>
  <div class="content">
    <p>We, citizens of India, are deeply concerned at the recent escalation of tensions between India and Pakistan, and at the climate of intolerance that is developing around it.</p>
    <p>Nothing can justify the act of terrorism that took the lives of more than 40 CRPF personnel in Pulwama on 14 February 2019. Nor can anything justify the Pakistan government’s covert support of armed groups that have carried out this sort of attack in Kashmir over the years.</p>
    <p>Nevertheless, India’s response to this incident must abide by international law in letter and spirit. It must also be responsible and take into account the risks involved in an armed conflict between two nuclear powers.</p>
    <p>The history of war across the world has repeatedly shown how easily and speedily an armed conflict can escalate well beyond what was initially expected. The first World War, we may recall, began with a single assassination, which set in motion a chain of attacks and counter-attacks that eventually led to millions of casualties. Similarly, it would be easy for seemingly “safe” strikes and counter-strikes between India and Pakistan to lead to a major confrontation, possibly even nuclear war, with disastrous consequences for both sides.</p>
    <p>Even a limited confrontation would resolve nothing – neither the tensions between India and Pakistan, nor the Kashmir dispute. On the contrary, it would aggravate tensions and delay the process of conflict resolution.</p>
    <p>The principal victims of this conflict are the civilian residents of Kashmir, who have endured immense suffering over the years, including gross human rights violations. In recent days, Kashmiris have also been a target of brutal attacks across the country. If the conflict intensifies, this hostility is likely to be extended to other minorities and dissidents.</p>
    <p>The victims also include hundreds of thousands of military and para-military personnel, largely drawn from underprivileged groups, who are exposed to the most trying service conditions in Kashmir and may soon be exposed to further danger.</p>
    <p>The victims of continued hostility also include the Indian population at large, in so far as it absorbs valuable financial and human resources that could be used for better purposes. Perhaps the biggest national damage is the erosion of democracy including the freedom of expression and dissent.</p>
    <p>Unfortunately, the climate of jingoism that tends to develop around this sort of situation is obscuring these simple truths. Even reasoned demands for a peaceful solution tend to be conflated with anti-national sentiment.</p>
    <p>We appeal to the governments on both sides to refrain from further hostilities, overt or covert, and to resolve their differences within the framework of international law and human rights.</p>
  </div>
  <div id="references">
    <h3>References</h3>
    <ul class="citation">
      <li>Article from 'The Wire' Website:<br><a href="https://thewire.in/rights/full-text-citizens-group-asks-india-pakistan-to-avoid-further-hostilities">https://thewire.in/rights/full-text-citizens-group-asks-india-pakistan-to-avoid-further-hostilities</a>
      <li>Discussion on Loomio:<br><a href="https://www.loomio.org/d/rEgcnaAd/citizens-group-asks-india-pakistan-to-avoid-further-hostilities">https://www.loomio.org/d/rEgcnaAd/citizens-group-asks-india-pakistan-to-avoid-further-hostilities</a></li>
    </ul>
  </div>
</section>
