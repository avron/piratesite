<div class="reveal">
  <!-- Any section element inside of this container is displayed as a slide -->
  <div class="slides">
    <section>
      <h2>Manifesto<br></h2>
      <ul>
        <br>
        <li><a href="https://www.loomio.org/d/RsMFfJ7l/what-is-our-position-on-the-use-of-the-death-penalty-" target="_blank">Abolish death penalty.</a></li>
        <li><a href="https://www.loomio.org/d/u2ZWgGHm/pirate-manifesto" target="_blank">Grant asylum to Edward Snowden.</a></li>
        <li><a href="https://www.loomio.org/d/rzkSPJvt/open-access-to-publicly-funded-research" target="_blank">Open Access to publicly funded research.</a></li>
        <li><a href="https://www.loomio.org/d/8PiJvU5E/add-to-manifesto-repeal-afspa" target="_blank">Repeal AFSPA.</a></li>
        <!--
             <li>Repeal 66A of IT act.</li>
             <li>Protect software from patents.</li>
             <li>Give teachers the same salary as a doctor like in Finland.</li>
        -->
      </ul>
      <br><br><br>
      <p><em>NOTE: </em>This Manifesto is still a work in progress. Go to the ongoing <a href="https://www.loomio.org/d/SJgKO4Hj/a-pirate-manifesto-for-india" target="_blank">loomio thread</a> to know more and contribute.</p>
      <br><br>
      <p class="nav-inst">INSTRUCTIONS: Use the navigation buttons on right or use left/right arrow keys to navigate between slides. If you are using a touch screen device you can swipe left and right.</p>
    </section>

    <section>
      <ul>
        <li><a href="https://www.loomio.org/d/i0Cz3x1S/add-to-manifesto-scrap-aadhar-in-a-phased-manner" target="_blank">
          Scrap Aadhaar in a phased manner:
          <ul>
            <li>Step 1: Make Aadhaar completely voluntary, remove even the categories allowed by Supreme Court.</li>
            <li>Step 2: Allow people to delete their Aadhaar data.</li>
            <li>Step 3: Stop new enrollments.</li>
            <li>Step 4: Scrap Aadhaar.</li>
          </ul>
        </a></li>
      </ul>
    </section>

    <section>
      <ul>
        <li><a href="https://www.loomio.org/d/QsVMv3Pb/proposal/vCOHy2wg" target="_blank">Create functional Police Complaints Authorities in every state to stop police abuse and bring accountability.</a></li>
      </ul>
      <ul>
        <li>
          <a href="https://www.loomio.org/d/le2pPFO3/proposal/SXqnGeVD" target="_blank">
            To restore trust in election process, we will ensure that:
            <ul>
              <li>a) All Electronic Voting Machines (EVMs) will have a Voter Verifiable Paper Trail (a receipt printed by the EVM which can be put into a ballot box).</li>
              OR
              <li>b) We will go back to paper ballots.</li>
            </ul>
          </a>
        </li>
      </ul>
    </section>
    <section>
      <ul>
        <li><a href="https://www.loomio.org/p/tFCKoeQ0/add-to-manifesto-make-marital-rape-a-criminal-offence-under-ipc-section-375" target="_blank">Make marital rape a criminal offence under IPC section 375</a></li>
      </ul>
    </section>
  </div>
</div>
