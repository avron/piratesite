<section id="election" class="page">
  <div class="row">
    <div class="col-md-12">

      <h2>Lok Sabha Election 2019</h2>

      <h3>Meet Indian Pirates Candidates</h3>
      <div class="row candidates">
        <div class="col-sm-6">
          <a href="https://poddery.com/u/praveen" target="_blank"><img class="img img-responsive thumbnail" src="<?php echo base_url(); ?>assets/img/pirate_praveen.jpg"></a>
          <p><b>Name</b>: <a href="https://poddery.com/u/praveen" target="_blank">Praveen Arimbrathodiyil</a></p>
          <p><b>Constituency</b>: Kannur</p>
        </div>
      </div>

      <h3>Meet the Candidates we support</h3>
      <div class="row candidates">
        <div class="col-sm-6">
          <a href="https://prakashraj.com/" target="_blank"><img class="img img-responsive thumbnail" width="250" height="250" src="<?php echo base_url(); ?>assets/img/prakash-raj.png"></a>
          <p><b>Name</b>: Prakash Raj</a></p>
          <p><b>Constituency</b>: Bangalore Central</p>
          <p><a href="https://prakashraj.com" target="_blank">Read more.</a></p>
        </div>
        <div class="col-sm-6">
          <img class="img img-responsive thumbnail" width="250" height="250" src="<?php echo base_url(); ?>assets/img/chinjuaswathi.jpeg">
          <p><b>Name</b>: Aswathi Rajappan</p>
          <p><b>Constituency</b>: Ernakulam, Kerala</p>
          <p><a href="https://www.thehindu.com/news/national/kerala/a-space-for-every-colour-in-the-rainbow/article26750770.ece">Read more.</a></p>
        </div>
        <div class="col-sm-6">
          <img class="img img-responsive thumbnail" width="250" height="250" src="<?php echo base_url(); ?>assets/img/kahaiyak.jpeg">
          <p><b>Name</b>: Kanhaiya Kumar</p>
          <p><b>Constituency</b>: Begusarai, Bihar</p>
          <p><a href="https://scroll.in/article/918339/kanhaiya-kumar-on-being-an-accidental-politician-and-why-he-is-contesting-the-lok-sabha-election">Read more.</a></p>
        </div>
        <div class="col-sm-6">
          <img class="img img-responsive thumbnail" width="250" height="250" src="<?php echo base_url(); ?>assets/img/gomathi.jpeg">
          <p><b>Name</b>: Gomathi</p>
          <p><b>Constituency</b>: Idukki, Kerala</p>
          <p><a href="https://www.opendemocracy.net/en/beyond-trafficking-and-slavery/women-strike-back-protest-of-pembillai-orumai-tea-workers/">Read more.</a></p>
        </div>
      </div>
      <h3>About Indian Pirates</h3>
      <p>No one asked us how we think this world should be. We should no longer accept what we supposedly cannot change. Instead, we need to change what we cannot accept. So we are remaking this world the way we think it should be - based on Equality, Democracy and Transparency!</p>
      <p>More about Indian Pirates:</p>
      <ul>
        <li>A platform of the people where everyone has equal voting rights and opportunities.</li>
        <li>Open to everyone.</li>
        <li>Transparent in all decision making.</li>
        <li>Stands for Human Rights & Social Justice.</li>
        <li>Everyone is a leader here.</li>
      </ul>
      <p>We recommend you to read the <a href="../../constitution" target="_blank">Indian Pirates Constitution</a> to know more.</p>

      <h3>Why we are contesting the Lok Sabha Election</h3>
      <ul class="questions">
        <li>Are you happy with the way things are in our society?</li>
        <li>What changes you want to see?</li>
        <li>Do you trust any parties to do it for you?</li>
        <li>Do you really think your leaders represent you?</li>
        <li>Why outsource your thinking?</li>
      </ul>
      <p>We're sure that you are so desperate to bring changes to the society. So are we! Don't hesitate anymore, join the Indian Pirates and bring the changes yourself.</p>

      <h3>What makes Indian Pirates different</h3>
      <p>Direct Democracy, Transparency and Loyalty to the Principles rather than to the leaders make the Indian Pirates different from the hundreds of political parties and groups out there.</p>
      <h4><b>Direct Democracy</b></h4>
      <p>Direct Democracy or Participatory Democracy means that every member has equal voice and say in any decision making. Direct democracy is one of the basic principles of the Indian Pirates. The structure is flat as compared to normal hierarchical structure, i.e. all the members are on the same level, no one is over or below. There will be no supreme authority which will make decision and also there will not be any blind followers. Here everyone has equal rights,  voice and say in every decision.</p>
      <h4><b>Transparency</b></h4>
      <p>The whole discussions and decisions of the Indian Pirates are publicly accessible. The discussions usually take place at online forum called Loomio.org. Anyone who visits the Indian Pirates group on the Loomio.org can view the discussions that went into each decision. There is nothing called closed door discussions or high level decisions.</p>
      <h4><b>Loyalty to Principles</b></h4>
      <p>The Indian Pirates expect loyalty to the basic principles and the constitution rather than the loyalty to the leaders or the party. This implies that no one should blindly follow what is said, rather question everything and if found anything violating the basic principles of the Indian Pirates, oppose them.</p>

      <h3>What we plan to do</h3>
      <p>We aim to spread our principles of Human Rights, Social Justice and Direct Democracy to the masses by contesting in the elections. Our main aim throughout the campaign will be to convey our ideas of how Indian Pirates are different from the existing political parties. Direct democracy and transparent discussions and policies being the key differentiators.</p>

      <h3>What we need</h3>
      <p>We are contesting the upcoming general elections to Lok Sabha and we need your support. We require money for the following:</p>
      <ul>
        <li>Security deposit - Rs. 25,000/- (without which we cannot contest).</li>
        <li>For creating publicity materials like pamphlets and posters.</li>
        <li>For covering basic expenses of volunteers.</li>
      </ul>

      <h3>How you can help</h3>
      <p>If you can't contribute money, that doesn't mean you can't be part of our campaign. What we need is your true support more than anything else. You can help by:</p>
      <ul>
        <li>Get the word out and make some noise about our campaign.</li>
        <li>Join us for online or even on-site campaigning.</li>
      </ul>
      <p>Read <a href="https://poddery.com/u/praveen" target="_blank">Pirate Praveen's</a> blog post for his motivations and background of Indian Pirates <a href="http://www.j4v4m4n.in/2016/04/14/kerala-assembly-elections-2016-and-my-candidature/" target="_blank">here</a>.</p>

      <h3 id="donate">Donate to the campaign</h3>
      <h4>Bank Account Details:</h4>
      <div class="row candidates">
        <div class="col-sm-6">
          <ul>
            <li><b>Name</b>: Praveen Arimbrathodiyil</li>
            <li><b>Account Number</b>: 116801502815</li>
            <li><b>Bank</b>: ICICI Bank</li>
            <li><b>Branch</b>: Kannur Talap</li>
            <li><b>IFSC</b>: ICIC0001168</li>
            <li><b>UPI</b>: diyil@icici</li>
          </ul>
        </div>
      </div>
      <h4>Via Our Democracy Crowd funding platform: <a href="https://www.ourdemocracy.in/Campaign/PraveenForKozhikode">https://www.ourdemocracy.in/Campaign/PraveenForKozhikode</a></h4>

      <h4>Financial contributors so far:</h4>

      <table class="table">
	<thead><th><td>Name</td><td>Amount</td></th></thead>
	<tbody><tr><td>Left over from Kerala Assembly Elections 2016</td><td>Rs. 5,296/-</td></tr>
	<tr><td>Jyothis Jagan</td><td>Rs. 500/-</td></tr>
	<tr><td>Rajeesh KV</td><td>Rs. 2500/-</td></tr>
	<tr><td>Aneesh A</td><td>Rs. 5000/-</td></tr>
	<tr><td>Priyadarshan KP</td><td>Rs. 500/-</td></tr>
	<tr><td>Via Our Democracy crowd funding page (as on 3rd May 2019)</td><td>Rs. 39200/-</td></tr>
	<tr><td>TOTAL</td><td>Rs.52,996/-</td></tr></tbody>
      </table>
      
      <h4>Expenses met:<br></h4>
      <table class="table">
	<thead><tr><td>Date</td><td>Expense</td><td>Amount</td></tr></thead>
	<tbody><tr><td>04<sup>th</sup> April 2019</td><td>Election Deposit</td><td>Rs. 25,000/-</td></tr>
	  <tr><td>11<sup>th</sup> April 2019</td><td> Co-Operative press (Notice)</td><td>Rs. 2,128/-</td></tr>
	  <tr><td>12<sup>th</sup> April 2019</td><td> Mahathma press (Visiting card)</td><td>Rs. 251/-</td></tr>
	  <tr><td>16<sup>th</sup> April 2019</td><td> Mahathma press (Visiting card)</td><td>Rs. 899/-</td></tr>
	  <tr><td>17<sup>th</sup> April 2019</td><td> Mahathma press (Multi-color Notice)</td><td>Rs. 3920/-</td></tr>
	  <tr><td>20<sup>th</sup> April 2019</td><td> Mahathma press (Notice)</td><td>Rs. 2867/-</td></tr>
	  <tr><td>05<sup>th</sup> May 2019</td><td> Our Democracy platform fees</td><td> Rs. 6108/-</td></tr></tbody>
      </table>
      <br>
      <ul>
        <li><strong>Campaigning Material Expense total</strong>: Rs 10065/-</li>
        <li><strong>Election Deposit</strong>: Rs. 25,000/-</li>
        <li><strong>Our Democracy platform fees</strong>: Rs. 6108/-</li>
      </ul>
      <ul>
        <li><strong>TOTAL</strong>: Rs.41,173/-</li>
        <li><strong>BALANCE</strong>: Rs.11,823/-</li>
      </ul>
      <h4>Other Contributions:</h4>
      <ul>
        <li>Design:</li>
        <ul>
          <li>Raghavendra Kamath</li>
          <li>Kannan V.</li>
          <li>Sruthi Chandran</li>
          <li>Rabeeu, Owl generation</li>
          <li>Abraham Raji</li>
        </ul>
      </ul>
      <ul>
        <li>Ground Campaigning:</li>
        <ul>
          <li>Praveen Arimbrathodiyil</li>
          <li>Sruthi Chandran</li>
          <li>Kannan VM</li>
          <li>Kelvin K</li>
          <li>Ambady</li>
          <li>Akhil Varkey</li>
          <li>Syam G Krishnan</li>
          <li>Hari</li>
        </ul>
      </ul>
      <ul>
        <li>Social Media</li>
        <ul>
          <li>Praveen Arimbrathodiyil</li>
        </ul>
      </ul>
      <ul>
        <li>Lok Sabha 2019 page</li>
        <ul>
          <li>Praveen Arimbrathodiyil</li>
          <li>Ambady Anand S.</li>
          <li>Abraham Raji</li>
        </ul>
      </ul>

      <h3>Contact us</h3>
      <p>If you have any suggestions/queries feel free to contact us at <a href="mailto:ahoy@pirates.org.in">ahoy@pirates.org.in</a></p>
    </div>
  </div>
</section>
</div>
<!--This closes the "body-container" div element opened in the header.php-->
